from app import db
from datetime import datetime, timedelta

class Users(db.Model):
	__tablename__="users"
	id = db.Column(db.Integer,primary_key=True)
	username = db.Column(db.String(50),unique=True)
	password = db.Column(db.String(50))
	email = db.Column(db.String(50))
	phone = db.Column(db.BigInteger)
	answered = db.Column(db.Integer,default=0)
	last_answered_time = db.Column(db.DateTime,default=(datetime.now()+timedelta(hours=5,minutes=30)), onupdate=(datetime.now()+timedelta(hours=5,minutes=30)))
	user_id = db.relationship('UserAnswers',backref='users',lazy='dynamic')
	user_identity = db.relationship('GameTime',backref='users',lazy='dynamic')

	def __init__(self,username,password,email,phone,last_answered_time):
		self.username = username
		self.password = password
		self.email = email
		self.phone = phone
		self.last_answered_time = last_answered_time

class Admin(db.Model):
	__tablename__="admin"
	id = db.Column(db.Integer,primary_key=True)
	username = db.Column(db.String(50),unique=True)
	password = db.Column(db.String(50))

	def __init__(self,username,password):
		self.username = username
		self.password = password


class Questions(db.Model):
	__tablename__="questions"
	id = db.Column(db.Integer,primary_key=True)
	description = db.Column(db.String(200),default='NULL')
	q_no = db.Column(db.Integer)
	no_of_images = db.Column(db.Integer)
	answer = db.Column(db.String(50))

	def __init__(self,q_no,no_of_images,answer):
		self.q_no = q_no
		self.no_of_images = no_of_images
		self.answer = answer


class UserAnswers(db.Model):
	__tablename__= "user_answers"
	id = db.Column(db.Integer,primary_key=True)
	q_no = db.Column(db.Integer)
	uid = db.Column(db.Integer,db.ForeignKey('users.id'))
	answer = db.Column(db.String(50))

	def __init__(self,q_no,uid,answer):
		self.q_no = q_no
		self.uid = uid
		self.answer = answer


class GameTime(db.Model):
	__tablename__ = "gametime"
	id = db.Column(db.Integer,primary_key=True)
	user_id = db.Column(db.Integer,db.ForeignKey('users.id'))
	start_time = db.Column(db.DateTime)
	end_time = db.Column(db.DateTime)

	def __init__(self,user_id):
		self.user_id = user_id
