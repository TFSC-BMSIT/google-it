from flask import render_template, request, flash, redirect, url_for, session
from app import app, db
from app.models import *
from app.forms import *

#MAXIMUM NUMBER OF QUESTIONS FOR A ROUND IS ASSIGNED HERE
max_questions = 20
time_interval = 1



def fetch_question_no(user):
	current_user = Users.query.filter_by(username=user).all()
	for u in current_user:
		question_no = u.answered
	return int(question_no)

@app.route("/")
def index():
	return render_template('index.html')


@app.route('/login',methods=['GET','POST'])
def login():
	if 'user' in session and session['user']:
		return redirect('/landing')
	else:
		form = LoginForm()
		if request.method == 'POST' and form.validate_on_submit():
			username = form.username.data
			password = form.password.data
			user = Users.query.filter_by(username=username).filter_by(password=password)
			c =0
			for u in user:
				c = c+1
			if c ==1:
				session['user'] = username
				for usr in user:
					session['uid'] = usr.id
				return redirect('/landing')
			else:
				flash("Email or Password incorrect")
				return render_template('login.html',form=form)
		else:
			return render_template('login.html',form=form)

@app.route('/register',methods=['GET','POST'])
def register():
	form = RegisterForm()
	if request.method == 'POST' and form.validate_on_submit():
		username = form.username.data
		email = form.email.data
		password = form.password.data
		phone = form.phone.data
		if Users.query.filter_by(username=username).count() >= 1:
			flash("Username already taken")
			return render_template('register.html',form=form)
		else:
			user = Users(username,password,email,phone,(datetime.now()+timedelta(hours=5,minutes=30)))
			db.session.add(user)
			db.session.commit()
			uid = Users.query.filter_by(username = username)
			for u in uid:
				user_id = u.id
				time = GameTime(user_id)
				db.session.add(time)
				db.session.commit()
			return redirect('/login')
	return render_template('register.html',form=form)


@app.route('/landing',methods=['GET','POST'])
def landing():
	next_question = fetch_question_no(session['user'])
	if next_question == (max_questions):
		return redirect('/finish')
	next_question += 1
	return render_template('landing.html',next_question=next_question)


@app.route('/question/<int:q_no>',methods=['GET','POST'])
def question(q_no):
	check_point = fetch_question_no(session['user'])
	if check_point != (q_no-1):
		return "Forbidden"
	else:
		if 'user' not in session and session['user']==False:
			return "Forbidden"
		if q_no == 1:
			time = GameTime.query.filter_by(user_id = session['uid']).all()
			for t in time:
				if not t.start_time:
					t.start_time = (datetime.now()+timedelta(hours=5,minutes=30))
					t.end_time = (datetime.now()+timedelta(hours=5,minutes=30)) + timedelta(hours = time_interval)
					db.session.commit()
		form = AnswerForm()
		end_time = GameTime.query.filter_by(user_id = session['uid']).all()
		for e in end_time:
			end = e.end_time
		image_no = Questions.query.filter_by(q_no=q_no).all()
		for num in image_no:
			images = num.no_of_images
		if request.method =='GET':
			next_question = fetch_question_no(session['user'])
			if next_question == (max_questions):
				return redirect('/finish')
			return render_template('question.html',form=form,q_no=q_no,images=int(images),end=end)
		if request.method == 'POST' and form.validate_on_submit():
			user_answer = form.answer.data
			ans = Questions.query.filter_by(q_no=q_no).all()
			answer_from_user = UserAnswers(q_no,session['uid'],user_answer)
			db.session.add(answer_from_user)
			db.session.commit()
			correct_answers = []
			for a in ans:
				correct_answers.append(a.answer)
			if user_answer.lower() in [c.lower() for c in correct_answers]:
				user = Users.query.filter_by(username=session['user']).all()
				for u in user:
					points = u.answered
					points +=1
					u.answered = points
					u.last_answered_time = datetime.now()+timedelta(hours=5,minutes=30)
					db.session.commit()
				next_question = fetch_question_no(session['user'])
				if next_question == (max_questions):
					return redirect('/finish')
				next_question +=1
				return redirect('/question/'+str(next_question))
			else:
				flash("Wrong Answer")
				return render_template('question.html',form=form,q_no=q_no,images=int(images),end=end)




@app.route('/finish',methods=['GET','POST'])
def finish():
	if 'user' in session and session['user']:
		my_answers = UserAnswers.query.filter_by(uid=session['uid'])
		points = Users.query.filter_by(id=session['uid'])
		return render_template('finish.html',my_answers=my_answers,points=points)
	return render_template('finish.html',my_answers=my_answers,points=points)




#ADMIN STUFF FROM HERE

@app.route('/admin',methods=['GET','POST'])
def admin():
	if 'admin' in session and session['admin']:
		return redirect('/admin_panel')
	else:
		form = AdminForm()
		if request.method == 'POST' and form.validate_on_submit():
			username = form.username.data
			password = form.password.data
			admin = Admin.query.filter_by(username=username).filter_by(password=password)
			c =0
			for u in admin:
				c = c+1
			if c ==1:
				session['admin'] = username
				for usr in admin:
					session['aid'] = usr.id
				return redirect('/admin_panel')
			else:
				flash("Email or Password incorrect")
				return render_template('admin_login.html',form=form)
		else:
			return render_template('admin_login.html',form=form)



@app.route('/admin_panel',methods=['GET','POST'])
def panel():
	if 'admin' in session and session['admin']:
		return render_template('admin_panel.html')
	else:
		return redirect('/admin')


@app.route('/scorecard',methods=['GET','POST'])
def scorecard():
	if 'admin' in session and session['admin']:
		users = Users.query.order_by(Users.answered.desc()).all()
		time = GameTime.query.all()
		return render_template('scorecard.html',users=users,time=time)
	else:
		return redirect('/admin')

@app.route('/all_answers',methods=['GET','POST'])
def all_answers():
	if 'admin' in session and session['admin']:
		allAns = UserAnswers.query.all()
		return render_template('all_answers.html',allAns=allAns)
	else:
		return redirect('/admin')

@app.route('/add_answer',methods=['GET','POST'])
def add_answer():
	if 'admin' in session and session['admin']:
		form = AddAnswers()
		if request.method == 'POST' and form.validate_on_submit():
			qid = form.qid.data
			img_no = form.img_no.data
			count = request.form.get("count")
			for i in range(1,int(count)+1):
				answer = request.form.get("answer"+str(i))
				question = Questions(qid,img_no,answer)
				db.session.add(question)
				db.session.commit()
			flash("Added")
			return render_template('add_answers.html',form=form)
		return render_template('add_answers.html',form=form)
	else:
		return redirect('/admin')



@app.route('/admin_logout')
def admin_logout():
	session.pop('admin',None)
	return redirect('/admin')

@app.route('/logout')
def logout():
	session.pop('user',None)
	return redirect('/login')
