from wtforms import StringField,PasswordField
from wtforms.validators import InputRequired,EqualTo,Email,Length
from flask_wtf import Form

class LoginForm(Form):
    username = StringField('Username',validators=[InputRequired()])
    password = PasswordField('password',validators=[InputRequired()])


class RegisterForm(Form):
    username = StringField('username',validators=[InputRequired()])
    email = StringField('Email',validators=[InputRequired(),Email()])
    phone = StringField('Phone number',validators=[InputRequired(),Length(min=10)])
    password = PasswordField('password',validators=[InputRequired(),EqualTo('confirm', message='Passwords must match'), Length(min=8)])
    confirm = PasswordField('password',validators=[InputRequired()])


class AnswerForm(Form):
    answer = StringField('answer',validators=[InputRequired()])

class AdminForm(Form):
    username = StringField('username',validators=[InputRequired()])
    password = PasswordField('password',validators=[InputRequired()])

class AddAnswers(Form):
    qid = StringField('question_id',validators=[InputRequired()])
    img_no = StringField('img_no',validators=[InputRequired()])
